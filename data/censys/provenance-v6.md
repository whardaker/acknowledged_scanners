# Censys Data - v6

Censys published a list of scanning IP's for v6 at https://support.censys.io/hc/en-us/articles/360043177092-Opt-Out-of-Scanning 

#IP's
Fetched: https://support.censys.io/hc/en-us/articles/360043177092-Opt-Out-of-Scanning

Date: 2022-03-24

- 2602:80d:1000:b0cc:e::
- 2620:96:e000:b0cc:e::
