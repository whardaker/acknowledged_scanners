# Censys Data

Censys publishes a list of scanning IP's at https://about.censys.io/

# IP's
Fetched: https://about.censys.io/

Date: 2021/09/24

- 192.35.168.0/23
- 162.142.125.0/24
- 74.120.14.0/24
- 167.248.133.0/24

Date: 2023/12/07
- 167.94.138.0/24
- 167.94.146.0/24
- 167.94.145.0/24 
